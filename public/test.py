from tkinter import *
from bs4 import BeautifulSoup
from tkinter import ttk
from tkinter import messagebox
import os
import re

global prozor_izmjena
global b1
global b2
global b3
global b4
global sadrzaj_unos
global soup


def doSomething():
    # check if saving
    # if not:
    prozor.destroy()
    try:
        prozor_izmjena.destroy()
    except:
        return



prozor=Tk()
naslovAplikacije=Label(prozor, text="Riječi u ", font=("Helvetica", 16))
naslovAplikacije.grid(row=0, sticky=W+E, pady=20)

def nova():
    if len(b1.get())==0:
        messagebox.showwarning("Upozorenje", "Trebate napisati natuknicu te njezin sadržaj.")
        return 
    if b1.get()[0].upper()=="A":
        nadi=soup.find("div", {"data-term":"A"})
        print (nadi)

def unesi():  
    global prozor_izmjena
    global b1
    global b2
    global b3
    global b4
    global sadrzaj_unos
    prozor_izmjena=Tk()
    l= Label(prozor_izmjena,text="Natuknica*: ")
    l.grid(row=1,column=0, sticky=W)
    b1 = Entry(prozor_izmjena,width=50)
    b1.insert(0, "")
    b1.grid(row=1, column=1)

    l= Label(prozor_izmjena,text="Sinonim: ")
    l.grid(row=2,column=0, sticky=W)
    b2 = Entry(prozor_izmjena,width=50)
    b2.insert(0, "")
    b2.grid(row=2, column=1)

    l= Label(prozor_izmjena,text="Antonim: ")
    l.grid(row=3,column=0, sticky=W)
    b3 = Entry(prozor_izmjena,width=50)
    b3.insert(0, "")
    b3.grid(row=3, column=1)
    
    l= Label(prozor_izmjena,text="Usporedba: ")
    l.grid(row=4,column=0, sticky=W)
    b4 = Entry(prozor_izmjena,width=50)
    b4.insert(0, "")
    b4.grid(row=4, column=1)

    l= Label(prozor_izmjena,text="Tekst za natuknicu*: ")
    l.grid(row=5,column=0, sticky=W)
    sadrzaj_unos=Text(prozor_izmjena,height=20, width=60)
    sadrzaj_unos.grid(row=6, padx=5, pady=5, columnspan=2)
    gumb23=ttk.Button(prozor_izmjena,text="unesi",command=nova)
    gumb23.grid(row=10, padx=5, pady=5, columnspan=2)
    prozor_izmjena.mainloop()
    return prozor_izmjena


#brisanje elementa liste
def brisi():
    trenutni_odabir=listbox.curselection()
    listbox.delete(trenutni_odabir)
    listbox.selection_set(trenutni_odabir)
    unos.delete(0, END)
    unos.insert(END, listbox.get(listbox.curselection()))
    broj_rijeci["text"]="broj riječi: "+str(listbox.size())

#dodvanje u entry box
def onselect(evt):
    unos.delete(0, END)
    unos.insert(END, listbox.get(listbox.curselection()))
     
#izmjena u listboxu 
def izmijena():
    broj=listbox.curselection()
    listbox.delete(broj)
    listbox.insert(broj,unos.get())
    listbox.selection_set(broj[0])

#dodavaje u listboxu 
def dodaj():
    broj=listbox.curselection()
    if broj==():
        broj=0
        listbox.insert(broj, unos.get())
    else:
        listbox.insert(broj[0]+1,unos.get())

    listbox.selection_clear(0, END)
    listbox.selection_set(broj[0]+1)
    broj_rijeci["text"]="broj riječi: "+str(listbox.size())

#sortiranje u listboxa
def sortiraj():
    all_items = listbox.get(0, END)
    listbox.delete(0, END)
    broj=0
    lista_s=list(all_items)
    lista_s.sort()
    for item in lista_s:
        listbox.insert(broj, item)
        broj=broj+1




##izrada sucelja
predaj = ttk.Button(prozor, text="unesi novu natuknicu",command=unesi)
predaj.grid(row=2, column=0,columnspan=1,padx=5,pady=5)

obrisi = ttk.Button(prozor, text="izmjeni odabranu natuknicu",command=brisi)
obrisi.grid(row=2, column=1,columnspan=1, padx=5,pady=5)

sortiraj = ttk.Button(prozor, text="obriši odabranu natuknicu",command=sortiraj)
sortiraj.grid(row=3, column=0,columnspan=4, padx=5,pady=5)

##otvaranje fajla
cwd = os.getcwd()
fajl="hr-eng.html"
soup=BeautifulSoup(open(str(cwd)+"/hr-eng.html",encoding="utf8"), "html.parser")
lista=[]
for x in soup.findAll('h4',{'class':"card__title"}):
    lista.append(x.text)
lista = set(lista)
lista = list(lista)
lista.sort()

##popis riječi
listbox = Listbox(prozor,width=50,height=20)
prozor.resizable(False, False)
broj=0
for item in lista:
    listbox.insert(broj, item)
    broj=broj+1

listbox.grid(row=0, column=0, columnspan=4,padx=5,pady=5)




#broj riječi
broj_rijeci= Label(prozor, text="broj riječi: "+str(listbox.size()))
broj_rijeci.grid(pady=10, row=5, column=0, columnspan=4, sticky=W+E)

##skroler
scrollbar = Scrollbar(prozor,orient="vertical")
scrollbar.grid(sticky=N+S+E, row = 0, rowspan = 25, column = 0,columnspan=4)
listbox.config(yscrollcommand=scrollbar.set)
listbox.bind('<<ListboxSelect>>', onselect)    
scrollbar.config(command=listbox.yview)

##kraj
prozor.protocol('WM_DELETE_WINDOW', doSomething)  # root is your root window
prozor.resizable(False, False)
prozor.mainloop()
